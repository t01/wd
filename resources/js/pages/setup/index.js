import { makeRoute } from '@/utils'

import Trello from './Trello.vue';

export default [
    makeRoute('/setup/trello', Trello),
  ];
