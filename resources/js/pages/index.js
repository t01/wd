import { makeRoute } from '@/utils'
import setup from './setup';

export default [
    ...setup,
    makeRoute('/*', { template: '<p>Placeholder {{ $route.path}}</p>' })
];
