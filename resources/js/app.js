import Vue from 'vue'
import App from '@/layout/App.vue'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import VueRouter from 'vue-router'

import 'vue-sidebar-menu/dist/vue-sidebar-menu.css'

import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

Vue.use(VueRouter)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

import routes from '@/pages'

new Vue({
  el: '#app',
  components: {
    app: App
  },
  router: new VueRouter({routes})
});
