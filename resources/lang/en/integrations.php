<?php

return [

    'labels' => [
        'trello' => 'Trello',
        'toggl' => 'Toggl',
        'slack' => 'Slack',
        'git' => 'Git'
    ]

];
