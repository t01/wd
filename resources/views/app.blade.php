<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <style>
        html { height: 100% }
        body { height: 100%; margin: 0px; padding: 0px }
        #app { width: 100%; height: 100%; }
    </style>
</head>
<body>
<div id="app">
    <app></app>
</div>
<script src="/js/app.js"></script>
</body>
</html>
