<?php

namespace App\Providers;

use App\Modules\Integrations\IntegrationsServiceProvider;
use Illuminate\Support\AggregateServiceProvider;

class ModulesServiceProvider extends AggregateServiceProvider
{
    protected $providers = [
        IntegrationsServiceProvider::class
    ];
}
