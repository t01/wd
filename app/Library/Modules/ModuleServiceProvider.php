<?php

namespace App\Library\Modules;

use Illuminate\Support\ServiceProvider;

abstract class ModuleServiceProvider extends ServiceProvider
{
    protected function repositories() : array
    {
        return [];
    }

    public function register()
    {
        foreach ($this->repositories() as $repository) {
            $this->app->singleton($repository);
        }
    }
}

