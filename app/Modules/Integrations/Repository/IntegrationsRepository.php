<?php

namespace App\Modules\Integrations\Repository;

use App\Library\Database\Repository;
use Illuminate\Support\Collection;

class IntegrationsRepository extends Repository
{
    public static function integrations() : Collection
    {
        return collect(config('integrations.available'))->mapWithKeys(function($service) {
            return [$service => trans("integrations.labels.$service")];
        });
    }
}
