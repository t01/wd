<?php

namespace App\Modules\Integrations;

use App\Library\Modules\ModuleServiceProvider;
use App\Modules\Integrations\Repository\IntegrationsRepository;

class IntegrationsServiceProvider extends ModuleServiceProvider
{
    protected function repositories() : array
    {
        return [
            IntegrationsRepository::class
        ];
    }
}
