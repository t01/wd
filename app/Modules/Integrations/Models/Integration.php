<?php

namespace App\Modules\Integrations\Models;

use Illuminate\Database\Eloquent\Model;

abstract class Integration  extends Model
{
    abstract protected $SERVICE;

    protected $table = 'integrations';

    public function __construct(array $authData)
    {
        parent::__construct([
            'service' => static::$SERVICE,
            'auth' => $authData
        ]);
    }
}
