<?php

namespace App\Http\Middleware;

use Closure;

class JsonResponse
{
    public function handle($request, Closure $next)
    {
        return response()->json($next($request));
    }
}
