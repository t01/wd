<?php

namespace App\Http\Controllers;

use App\Modules\Integrations\Repository\IntegrationsRepository;

class IntegrationController extends Controller
{
    /** @var IntegrationsRepository */
    private $integrations;

    /**
     * IntegrationController constructor.
     */
    public function __construct(IntegrationsRepository $integrations)
    {
        $this->integrations = $integrations;
    }

    public function index() {
        return IntegrationsRepository::integrations();
    }
}
